﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci3_z2
{
    class Student
    {
        string ime;
        string prezime;
        string brojIndeksa;
        int maksimalniBrojIspita;
        int brojPolozenihIspita;
        int[] nizOcena;


        public Student(int brojPolozenihIspita)
        {
            this.maksimalniBrojIspita = brojPolozenihIspita;
            this.nizOcena = new int[brojPolozenihIspita];
        }

        public string BrojIndeksa { get { return this.brojIndeksa; } }
        public int BrojPolozenihIspita { get { return this.brojPolozenihIspita; } }

        public void UpisOceneSaIspita(int ocena)
        {
            if(this.brojPolozenihIspita < this.maksimalniBrojIspita)
            {
                this.nizOcena[this.brojPolozenihIspita] = ocena;
                Console.WriteLine($"Dodata je ocena {ocena}.");
                this.brojPolozenihIspita++;
            }
        }


        public void Ucitavanje(string ime, string prezime, string brojIndeksa,
            int brojPolozenihIspita, int[] nizOcena)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.brojIndeksa = brojIndeksa;
            this.brojPolozenihIspita = brojPolozenihIspita;
            this.nizOcena = nizOcena;
        }

        public void Prikaz()
        {
            Console.WriteLine($"Student: Ime: {this.ime} Prezime: {this.prezime} br.indeksa:" +
                $"{this.brojIndeksa}" +
                $" polozio je {this.brojPolozenihIspita} ispita.");
            Console.WriteLine("Ocene:");
            for(int i = 0; i < this.brojPolozenihIspita; i++)
            {
                Console.Write("{0} ", this.nizOcena[i]);
            }
            Console.WriteLine();
        }
    }
}
