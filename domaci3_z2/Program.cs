﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci3_z2
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student(30);
            int[] nizOcena = new int[30];
            Console.WriteLine("Ime:");
            string ime = Console.ReadLine();
            Console.WriteLine("Prezime:");
            string prezime = Console.ReadLine();
            Console.WriteLine("Broj indeksa:");
            string brojIndeksa = Console.ReadLine();
            Console.WriteLine("Broj polozenih ispita:");
            int.TryParse(Console.ReadLine(), out int brojPolozenihIspita);
            Console.WriteLine("Ocene polozenih ispita:");
            for(int i = 0; i < brojPolozenihIspita; i++)
            {
                Console.Write($"{i + 1}. ocena: ");
                int.TryParse(Console.ReadLine(), out nizOcena[i]);
            }
            Console.WriteLine();
            s.Ucitavanje(ime,prezime,brojIndeksa,brojPolozenihIspita,nizOcena);
            s.Prikaz();
            Console.WriteLine();
            s.UpisOceneSaIspita(8);
            s.Prikaz();

        }
    }
}
